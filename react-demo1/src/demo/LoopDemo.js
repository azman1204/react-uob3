import { useState } from "react";

function LoopDemo() {
    //const arr = [1,2,3,4,5,6,7,8,9,10];
    let [arr, setArr] = useState([1,2,3]);

    function doInput(event) {
        let value = parseInt(event.target.value);
        // lesson 1: before set create a new value
        setArr([...arr, value]);
        //setArr(arr2);
        console.log(arr);
        //arr.splice(index, 1)
    }

    function doDelete(index) {
        let arr2 = [...arr];
        arr2.splice(index,1);
        setArr(arr2);
    }

    return (
        <div>
            <input type="text" onBlur={doInput} />

            {
                arr.map((item, index) => (
                    <li key={index} onDoubleClick={()=>doDelete(index)}>{index} {item}</li>
                ))
            }
        </div>
    );
}

export default LoopDemo;