import { useState } from "react";

function ShowHide() {
    let [showMessage, setShowMessage] = useState(false);
    //let showMessage = false;

    function doClick() {
        showMessage = ! showMessage;
        setShowMessage(showMessage);
        console.log(showMessage);
    }

    return (
        <div>
            { showMessage && <p>Hello World</p> }
            <button onClick={doClick} className="btn btn-primary">Show / Hide</button>
        </div>
    );
}

// import ShowHide from './ShowHide';
// import { Hello } from './ShowHide';

export function Hello() {
    console.log('Hello');
}

export default ShowHide;