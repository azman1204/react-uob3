import { useState } from "react";

function Counter() {
    // useState to indicate which part to refresh
    let [no, setNo] = useState(10); 
    //const no2 = 0;

    function increment() {
        no++;
        setNo(no);
        console.log(no);
    }

    function decrement() {
        no--;
        setNo(no);
        console.log(no);
    }

    return (
        <div>
            No: {no}
            <br />
            <button onClick={increment}>+</button>
            <button onClick={decrement}>-</button>
        </div>
    );
}

export default Counter;