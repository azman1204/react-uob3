import './App.css';
import Home from './Home';
import Greeting from './Greeting';
import Greeting2 from './Greeting2';

function App() {
  // var, let, const
  const name = 'Azman';

  return (
    <div className="App">
      <h1>Hello World { name }</h1>
      { 5 + 5 }
      <Home />
      <Greeting name="John Doe" age="45" />
      <Greeting2 name="Abu" />
      <Greeting2 name="John Doe" />
    </div>
  );
}

export default App;
