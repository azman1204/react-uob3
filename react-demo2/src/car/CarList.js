import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import setting from "../setting";
import { useAuth } from "../AuthContext";
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

function CarList() {
    let [cars, setCars] = useState([]);
    let [pagination, setPagination] = useState({totalPages:0});
    let [page, setPage] = useState(1);
    let [ok, setOk] = useState(true);
    let { token } = useAuth();
    let headers = {'Authorization':  'Bearer ' + token, 'Content-Type': 'application/json'};

    // useEffect, work like constructor
    useEffect(() => {
        fetchCar(1);
    }, []);

    function fetchCar(page) {
        fetch(setting.url + '/car-search?page=' + page, {
            method: 'GET', 
            headers: headers
        })
        .then(response => response.json())
        .then(json => {
            console.log(json.body);
            setCars(json.body);
            setPagination(json.pagination);
        }).catch(error => {
            console.log(error);
            setOk(false);
        });
    }

    function deleteMe(id) {
        console.log("id = ", id);
        let yes = window.confirm('Are you sure ?');
        if (yes) {
            // delete the data
            let url = 'http://localhost:8080/car-delete/' + id;
            let setting = { method: 'DELETE', headers: headers};
            fetch(url, setting)
            .then(response => response.json())
            .then(json => {
                console.log(json);
                fetchCar();
            })
            .catch(err => console.log(err));
        }
    }

    function handleChange(Event, value) {
        console.log("value", value);
        setPage(value);
        fetchCar(value);
    };

    function printCar() {
        let no = (page - 1) * 2 + 1;
        return (
            <div>
                <table className="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Brand</td>
                            <td>Model</td>
                            <td>Color</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    { cars.map((item, index) => (
                        <tr>
                            <td>{no++}</td>
                            <td>{item.brand}</td>
                            <td>{item.model}</td>
                            <td>{item.color}</td>
                            <td>
                                <button onClick={()=>deleteMe(item.id)} className="btn btn-outline-danger">Delete</button>
                                <Link to={`/car-edit/${item.id}`} className="btn btn-outline-success ms-1">Update</Link>
                            </td>
                        </tr>
                    )) }
                    </tbody>
                </table>

                <Stack spacing={2}>
                    <Pagination count={pagination.totalPages} onChange={handleChange} defaultPage={1} boundaryCount={2} />
                </Stack>
            </div>);
    }

    if (! ok) {
        return <p className="alert alert-danger">Error during loading data..</p>
    }

    return (
        <div>
            {printCar()}
        </div>
    );
}

export default CarList;