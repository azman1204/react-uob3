import { useEffect, useRef } from "react";
import { useNavigate, useParams } from "react-router-dom";
import setting from "../setting";
import { useAuth } from "../AuthContext";

function CarForm({isUpdate, id}) {
    
    let model  = useRef();
    let brand  = useRef();
    let year   = useRef();
    let registrationNumber  = useRef();
    let price  = useRef();
    let navigate = useNavigate();
    let { id2 } = useParams();
    let { token } = useAuth();
    let headers = {
        method: 'GET',
        headers: {
            'Authorization':  'Bearer ' + token, 
            'Content-Type': 'application/json'
        }
    };

    useEffect(() => {
        if (isUpdate) {
            console.log('update car');
            fetch(setting.url + '/car/' + id2, headers)
            .then(body => body.json())
            .then(json => {
                let car = json.body;
                model.current.value = car.model;
                brand.current.value = car.brand;
                registrationNumber.current.value = car.registerNumber;
                price.current.value = car.price;
                year.current.value = car.year;
            })
            .catch();
        } else {
            console.log("new car")
        }
    }, []);

    function save() {
        let data = {
            model: model.current.value,
            brand: brand.current.value,
            year: year.current.value,
            registerNumber: registrationNumber.current.value,
            price: price.current.value
        };

        headers.body = JSON.stringify(data);

        if (isUpdate) {
            var url = 'http://localhost:8080/car-update/' + id2;
            headers.method = 'PUT';
        } else {
            var url = 'http://localhost:8080/car-save';
            headers.method = 'POST';
        }
        
        fetch(url, headers)
        .then(response => response.json())
        .then(json => {
            console.log(json);
            navigate('/car-list');
        });
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-1">Brand</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={brand} />
                </div>
            </div>
            <div className="row mt-1">
                <div className="col-md-1">Model</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={model} />
                </div>
            </div>
            <div className="row mt-1">
                <div className="col-md-1">Registration Number</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={registrationNumber} />
                </div>
            </div>
            <div className="row mt-1">
                <div className="col-md-1">Price</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={price} />
                </div>
            </div>
            <div className="row mt-1">
                <div className="col-md-1">Year</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={year} />
                </div>
            </div>
            <div className="row mt-1">
                <div className="col-md-1"></div>
                <div className="col-md-5">
                    <button className="btn btn-primary" onClick={save}>Save</button>
                </div>
            </div>
        </div>        
    );
}

export default CarForm;