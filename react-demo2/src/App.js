import CarList from './car/CarList';
import CarForm from './car/CarForm';
import FetchDemo from './demo1/FetchDemo';
import FetchDemo2 from './demo1/FetchDemo2';
import Menu from './demo1/Menu';
import NotFound from './demo1/NotFound';
import EditPost from './post/EditPost';
import FormPost from './post/FormPost';
import ListPost from './post/ListPost';
import Login from './Login';
import { useAuth } from './AuthContext';
import Registration from './Registration';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import CustomHookDemo from './hook/CustomHookDemo';
import ButtonDemo from './mui/ButtonDemo';
import UploadFile from './mui/UploadFile';
import UploadList from './mui/UploadList';
import Profile from './mui/Profile';

function App() {
  const { authenticated } = useAuth();
    
  if (authenticated) {
    return (
      <Router>
        <Menu />
        <div className='container mt-4'>
          <Routes>
            <Route path='/' element={<FetchDemo />} />
            <Route path='/post-list' element={<ListPost />} />
            <Route path='/post-create' element={<FormPost />} />
            <Route path='/post-edit/:id2' element={<EditPost />} />
            <Route path='/car-list' element={<CarList />} />
            <Route path='/car-new' element={<CarForm />} />
            <Route path='/car-edit/:id2' element={<CarForm isUpdate="true" />} />
            <Route path='/mui-button' element={<ButtonDemo />} />
            <Route path='/mui-upload' element={<UploadFile />} />
            <Route path='/mui-upload-list' element={<UploadList />} />
            <Route path='/profile' element={<Profile />} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </div>
      </Router>
    );
  } else {
    return (
      <Router>
        <div className='container mt-4'>
          <Routes>
            <Route path='/' element={<Login />} />
            <Route path='/custom-hook' element={<CustomHookDemo />} />
            <Route path='/registration' element={<Registration />} />
            <Route path='*' element={<NotFound />} />
          </Routes>
        </div>
      </Router>
    );
  }
}

export default App;
