import { Button } from "@mui/material";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import iguana from '../static/images/contemplative-reptile.jpg';

function ButtonDemo() {
  const ids = [1,2,3,4,5];
  const rendered = [];
  for (let  id of ids) {
    rendered.push(<li>{id}</li>);
  }

  const rendered2 = [];
  for (let  id of ids) {
    rendered2.push(
    <tr>
      <td>{id}</td>
      <td>test</td>
    </tr>);
  }

  return (
    <div>
      <Button style={{margin:'5px'}}>First Button</Button>
      <Button style={{margin:'5px'}} variant="contained">Contained</Button>
      <Button variant="outlined">Outlined</Button>
      {/* <img src="http://localhost:8080/images/master_class.png" /> */}
      <div className="card m-4" style={{width:'50%'}}>
        <div className="card-body">
        {rendered}
        </div>
      </div>
      
      <table className="table table-bordered">
        <tbody>
          {rendered2}
        </tbody>
      </table>

      <Card sx={{ maxWidth: 345 }}>
        <CardMedia
          sx={{ height: 140 }}
          image={iguana}
          title="green iguana.."
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Lizard
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Lizards are a widespread group of squamate reptiles, with over 6,000
            species, ranging across all continents except Antarctica
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small">Share</Button>
          <Button size="small">Learn More</Button>
        </CardActions>
      </Card>
    </div>
  );
}

export default ButtonDemo;
