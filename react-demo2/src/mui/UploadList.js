import { useEffect, useState } from "react";
import { useAuth } from "../AuthContext";

function UploadList() {
    let { token } = useAuth();
    let headers = {'Authorization':  'Bearer ' + token};
    let [images, setImages] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/upload-list', {headers: headers})
        .then(body => body.json())
        .then(json => setImages(json))
        .catch(err => console.log(err));
    }, []);

    return (
        <div>
            { images.map((item, index) => (
                <li><img src={ 'http://localhost:8080/images/' + item.fileName } alt="img" /></li>
            ))}
        </div>
    );
}

export default UploadList;