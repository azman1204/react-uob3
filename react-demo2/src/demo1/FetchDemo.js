import { useState, useEffect } from "react";

function FetchDemo() {
    let [post, setPost] = useState();

    // useEffect, work like constructor
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/todos/20')
            .then(response => response.json())
            .then(json => {
                console.log(json);
                setPost(json);
            });
    }, []);

    function printPost() {
        return (
            <div>
                <p>Title   : {post.title}</p>
                <p>User ID : {post.userId}</p>
                <p>ID      : {post.id}</p>
                <p>Completed : {post.completed}</p>
            </div>);
    }

    return (
        <div>
            { post && printPost() }
        </div>
    );
}

export default FetchDemo;