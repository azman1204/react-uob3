import { useState, useEffect } from "react";

function FetchDemo2() {
    let [post, setPost] = useState([]);

    // useEffect, work like constructor
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => {
                console.log(json);
                setPost(json);
            });
    }, []);

    function printPost() {
        return (
            <div>
                <table className="table table-bordered table-striped table-hover">
                    <thead >
                        <tr>
                            <td>No.</td>
                            <td>User ID</td>
                            <td>ID</td>
                            <td>Title</td>
                            <td>Body</td>
                        </tr>
                    </thead>
                    <tbody>
                    { post.map((item, index) => (
                        <tr>
                            <td>{index + 1}</td>
                            <td>{item.userId}</td>
                            <td>{item.id}</td>
                            <td>{item.title}</td>
                            <td>{item.body}</td>
                        </tr>
                    )) }
                    </tbody>
                </table>
            </div>);
    }

    return (
        <div>
            {/* { post && printPost() } */}
            { post.length === 0 ? <p>Loading...</p> : printPost() }
        </div>
    );
}

export default FetchDemo2;