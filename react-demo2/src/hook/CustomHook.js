import { useEffect, useState } from "react";

const useApiData = (url) => {
    let [data, setData] = useState([]);
    let [error, setError] = useState();

    useEffect(() => {
        console.log(url);
        fetch(url)
        .then(body => {
            if (! body.ok) {
                // 400, 500
                console.log('not ok');
                setError('Technical error..');
            } else {
                return body.json();
            }
        })
        .then(json => setData(json.body))
        .catch(err => {
            console.log(err);
            setError(err.message);
        });
    }, [url]);

    return {data, error};
}

export default useApiData;