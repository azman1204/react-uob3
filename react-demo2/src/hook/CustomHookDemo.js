import useApiData from "./CustomHook";

function CustomHookDemo() {
    let url = 'http://localhost:8080/car-search';
    const {data, error} = useApiData(url);

    if (error) {
        return <p>Error : {error}</p>
    }

    return (
        <p>
            {
            data.map(item => (
                <li>{item.model}</li>
            ))
            }
        </p>
    );
}

export default CustomHookDemo;