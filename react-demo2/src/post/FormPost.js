import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import setting from "../setting";

function FormPost() {
    let author = useRef();
    let title  = useRef();
    let navigate = useNavigate();

    function save() {
        console.log(author.current.value, title.current.value);
        let url = setting.url2 + '/posts';
        let data = {author: author.current.value, title: title.current.value};
        let setting = {
            method: 'post', 
            body: JSON.stringify(data),
            headers: {'Content-type': 'application/json'}
        };
        fetch(url, setting)
        .then(response => response.json())
        .then(json => console.log(json));
        navigate('/post-list');
    }

    return (
        <div>
            <div className="row">
                <div className="col-md-1">Title</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={title} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-1">Author</div>
                <div className="col-md-5">
                    <input type="text" className="form-control" ref={author} />
                </div>
            </div>
            <div className="row">
                <div className="col-md-1"></div>
                <div className="col-md-5">
                    <button className="btn btn-primary" onClick={save}>Save</button>
                </div>
            </div>
        </div>        
    );
}

export default FormPost;