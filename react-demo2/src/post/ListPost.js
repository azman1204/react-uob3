import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import setting from "../setting";

function ListPost() {
    let [post, setPost] = useState([]);
    let [ok, setOk] = useState(true); // load data ok by default

    // useEffect, work like constructor
    useEffect(() => {
        fetchPost();
    }, []);

    function fetchPost() {
        fetch(setting.url2 + '/posts')
            .then(response => response.json())
            .then(json => {
                console.log(json);
                setPost(json);
            }).catch(err => {
                console.log(err);
                setOk(false);
            });
    }

    function deleteMe(id) {
        console.log("id = ", id);
        let yes = window.confirm('Are you sure ?');
        if (yes) {
            // delete the data
            let url = setting.url2 + '/posts/' + id;
            let setting = {
                method: 'DELETE'
            };
            fetch(url, setting)
            .then(response => response.json())
            .then(json => console.log(json));
            fetchPost();
        }
    }

    function printPost() {
        return (
            <div>
                <table className="table table-bordered table-striped table-hover">
                    <thead>
                        <tr>
                            <td>No.</td>
                            <td>Title</td>
                            <td>Author</td>
                            <td>ID</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    { post.map((item, index) => (
                        <tr>
                            <td>{index + 1}</td>
                            <td>{item.title}</td>
                            <td>{item.author}</td>
                            <td>{item.id}</td>
                            <td>
                                <button onClick={()=>deleteMe(item.id)} className="btn btn-outline-danger">Delete</button>
                                <Link to={`/post-edit/${item.id}`} className="btn btn-outline-success ms-1">Update</Link>
                            </td>
                        </tr>
                    )) }
                    </tbody>
                </table>
            </div>);
    }

    if (! ok) {
        return <p className="alert alert-danger">Error loading data</p>
    }

    return (
        <div>
            {/* { post && printPost() } */}
            { post.length === 0 ? <p>Loading...</p> : printPost() }
        </div>
    );
}

export default ListPost;